# Welcome to YouPlay

Do you like music but not restrictions?

With Youplay you can search for whatever music song in your mind. It will list a number of matching songs. You can listen to it and it will be downloaded as an mp3 file.

Youplay is a single Python file with little dependencies. It is also suitable for beginners, who want to learn Python.

## Installation

Since I did not manage to create a Faltpak, Snap or Deb package, you have to install it manually, which is quite easy. Youplay consists of these files:

* youplay.py - the python 3 script

* youplay.svg - the app icon

* youplay.desktop - the GNOME launcher

* README.md - this file

Just download the files to a dedicated folder, eg. /home/username/youplay/. Make youplay.py executable (chmod +x youplay.py). Change the pathes in youplay.desktop to your pathes. There are lines for Exec, Icon and Path, that must be adapted to your local directories. Copy the file youplay.desktop to /home/user/.local/share/applications/ to make it visible to the launcher of your desktop environment. Check if the launcher icon is able to start the program.

## Dependencies

YouPlay has some dependencies, which are:

* python3 (required to run a python script)
  should already be part of your distribution

* mpv (required for the CLI player)
  install with: sudo apt install mpv

* python-mpv (required for the GUI player)
  install with: pip3 install --upgrade python-mpv

* youtube-dl (required to search and download music from Youtube)
  install with: pip3 install --upgrade youtube-dl

* ffmpeg (required to calculate the duration of mp3 files)
  install with: sudo apt install ffmpeg

Please install these dependencies before you start YouPlay. Maybe *ffmpeg* and *mpv* is already installed in your GNU/Linux environment.

## Operation Instructions

YouPlay supports two modes: CLI and GUI

You can start YouPlay in CLI mode like this:

* ./youplay.py

* ./youplay.py songtitle

All other options will be shown interactively. In this mode, the audio player MPV will be shown in CLI-mode as well.

You can also start YouPlay in GUI mode:

* ./youplay.py gui
* youplay.desktop (with adapted pathes and copied to /home/user/.local/share/applications/)

When the GUI shows up, you can enter a song title that you want to listen to. The App will present a list of 10 matching songs from Youtube. Select one or doubleclick to start downloading and playing it. In GUI mode, YouPlay will start MPV in internal mode (no GUI). All songs that you are listened to, are stored as mp3 files in the 'music' subfolder.

A song, that was already downloaded, will not be downloaded again, as long as the file exists in the 'music' subfolder. You can show and play all downloaded songs with the SONG button. Just enter another search term to return to the search list.

## Flatpak

### Preparations
Make sure that your distribution uses the flathub repository:
```
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

Building a flatpak package is done by the `flatpak-builder` tool. In most distributions, it is either preinstalled or can be added via the software administration - in Ubuntu, for example, via the `flatpak-builder` package. Alternatively, you can install the tool as a flatpak package:
```
flatpak install flathub org.flatpak.Builder
```
The `flatpak-builder` creates a sandbox with the selected runtime and compiles your source code in it. To compile a programme, there is an SDK for each runtime with all developer files, compilers and debuggers.
```
flatpak install flathub org.gnome.Platform//3.38 org.gnome.Sdk//3.38 org.freedesktop.Platform//20.08
```
### Build
The `flatpak-builder` first creates the directory `fp_build` in which it builds the flatpak application. If necessary, it downloads the archives specified in the manifest file or copies the source code. Then the tool builds each module specified in the manifest.
```
flatpak-builder fp_build ch.rum3ber.youplay.json --force-clean
```
You can then start the Flatpak application on a test run: 
```
flatpak-builder --run fp_build ch.rum3ber.youplay.json run.sh
```
## Release Notes

###### Version 0.25 - 2021.01.04

* Code clean-up

* Mutagen dependency removed

###### Version 0.24 - 2021.01.03

* GUI song number centered

* Only one button to PLAY or STOP a song

* Doubleclick on a song while another one is playing, will stop the playing song and play the doubleclicked song

* Progress bar works when a second song is searched

* The SONG-button will show and let you play all downloaded songs from the 'music' folder

###### Version 0.23 - 2020.12.23

* GUI mode shows playing time

* Columns and song titles are correctly adjusted to fit into window

###### Version 0.22 - 2020.12.21

* GUI mode will utilize the internal python-mpv player instead of the external mpv player

* PLAY-button will be disabled if you doubleclick song title or click the PLAY-button

###### Version 0.21 - 2020.12.19 (my birthday version)

* ffmpeg dependency removed, because youtube-dl will download mp3 directly

* Downloaded songs will be stored in the 'music' subfolder

* Already downloaded songs will not be downloaded again, but played immediately

* Option to save a song removed from CLI and GUI, because it is obsolete

* PLAY button disabled when song is playing (only when started from PLAY button, not from doubleclick)

###### Version 0.20 - 2020.12.18

* Info about song duration added to CLI and GUI

* Endless streams are omitted (by duration = 0)

* Number of songs reduced to 10 (to avoid lenghty loading time)

###### Version 0.19 - 2020.12.16

* Progress bar while searching, downloading and saving songs
* Progress bar for CLI and GUI mode

###### Version 0.18 - 2020.12.15

* youplay.desktop-file doesn't require bash-script, but call youplay.py directly

* CLI-mode utilises MPV in CLI-mode

* GUI-mode starts MPV in GUI-mode

* Doubleclick in song list, starts download and playback of selected song

## Todo

* avoid youplay.desktop to launch two icons

* 

* add pause to GUI mode

## Licensing

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/youplay.git

## Contact

https://matrix.to/#/@ralfhersel:feneas.org
